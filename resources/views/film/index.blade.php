@extends('layout.master')

@section('judul')
    List Film
@endsection

@section('subjudul')
    Menampilkan seluruh film
@endsection

@section('content')
@auth
<a href="/film/create" class="btn btn-primary my-3"> Tambah Film </a>
@endauth

<div class="row">
    @forelse ($film as $item)
            <div class="col-4">
                <div class="card">
                    <img src="{{asset('image/'.$item->poster)}}" width="100%" height="300px" alt="...">
                    <div class="card-body">
                        <h3>{{$item->judul}}</h3>
                        <span class ="badge badge-primary">{{$item->genre->nama}}</span>
                        <p class="card-text">{{Str::limit($item->ringkasan,100)}}</p>
                        @auth
                        <form action="/film/{{$item->id}}" method="POST">
                        @csrf
                        @method('delete')
                            <a href="/film/{{$item->id}}" class="btn btn-primary">Detail</a>
                            <a href="/film/{{$item->id}}/edit" class="btn btn-warning">Edit</a>
                            <input type="submit" value="Delete" class="btn btn-danger">
                        </form>
                        @endauth
                        @guest
                        <a href="/film/{{$item->id}}" class="btn btn-primary">Detail</a>

                        @endguest
                    </div>
                </div>
            </div>    
    @empty
    <h1> Tidak Ada Film</h1>
    @endforelse
</div>
@endsection
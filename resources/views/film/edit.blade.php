@extends('layout.master')

@section('judul')
    Edit Film
@endsection

@section('subjudul')
    Halaman untuk mengubah data film
@endsection

@section('content')
<form action="/film/{{$film->id}}" method="POST" enctype="multipart/form-data">
    @csrf
    @method('put')
        <div class="form-group">
            <label>Judul Film</label>
            <input value="{{$film->judul}}" type="text" class="form-control" name="judul">
        </div>
        @error('judul')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror
        <div class="form-group">
            <label>Ringkasan</label>
            <textarea name="ringkasan" class="form-control">{{$film->ringkasan}}</textarea>
        </div>
        @error('ringkasan')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror
        <div class="form-group">
            <label>Tahun</label>
            <input type="number" class="form-control" value="{{$film->tahun}}" name="tahun">
        </div>
        @error('tahun')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror
        <div class="form-group">
            <label>Poster</label>
            <input type="file" class="form-control" name="poster">
        </div>
        @error('poster')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror
        <div class="form-group">
            <label>Genre</label>
            <select name="genre_id" class="form-control">
                <option value="">---Pilih Genre---</option>
                @foreach ($genre as $item)
                    @if ($item->id === $film->genre_id)
                        
                        <option value="{{$item->id}}" selected>{{$item->nama}}</option>
                    @else
                        <option value="{{$item->id}}">{{$item->nama}}</option>    

                    @endif
                    
                @endforeach
            </select>
        </div>
        <button type="submit" class="btn btn-primary">Edit</button>
    </form>
@push('script')
<script src="https://cdn.tiny.cloud/1/pqkxqtnu27m5t6p8nk3hkq3qhrgnhot3xblyo2u8xwwutdn6/tinymce/5/tinymce.min.js" referrerpolicy="origin"></script>
    <script>
        tinymce.init({
          selector: 'textarea',
          plugins: 'a11ychecker advcode casechange export formatpainter linkchecker autolink lists checklist media mediaembed pageembed permanentpen powerpaste table advtable tinycomments tinymcespellchecker',
          toolbar: 'a11ycheck addcomment showcomments casechange checklist code export formatpainter pageembed permanentpen table',
          toolbar_mode: 'floating',
          tinycomments_mode: 'embedded',
          tinycomments_author: 'Author name',
        });
    </script>
@endpush
@endsection
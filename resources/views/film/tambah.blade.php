@extends('layout.master')

@section('judul')
    Tambah Film
@endsection

@section('subjudul')
    Halaman untuk menambahkan data film
@endsection

@section('content')
<div>
    <form action="/film" method="POST" enctype="multipart/form-data">
    @csrf
        <div class="form-group">
            <label>Judul Film</label>
            <input type="text" class="form-control" name="judul">
        </div>
        @error('judul')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror
        <div class="form-group">
            <label>Ringkasan</label>
            <textarea name="ringkasan" class="form-control"></textarea>
        </div>
        @error('ringkasan')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror
        <div class="form-group">
            <label>Tahun</label>
            <input type="number" class="form-control" name="tahun">
        </div>
        @error('tahun')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror
        <div class="form-group">
            <label>Poster</label>
            <input type="file" class="form-control" name="poster">
        </div>
        @error('poster')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror
        <div class="form-group">
            <label>Genre</label>
            <select name="genre_id" class="form-control">
                <option value="">---Pilih Genre---</option>
                @foreach ($genre as $item)
                    <option value="{{$item->id}}">{{$item->nama}}</option>
                @endforeach
            </select>
        </div>
        
        
        
        
        
        
        <button type="submit" class="btn btn-primary">Tambah</button>
    </form>
</div>
@push('script')
<script src="https://cdn.tiny.cloud/1/pqkxqtnu27m5t6p8nk3hkq3qhrgnhot3xblyo2u8xwwutdn6/tinymce/5/tinymce.min.js" referrerpolicy="origin"></script>
    <script>
        tinymce.init({
          selector: 'textarea',
          plugins: 'a11ychecker advcode casechange export formatpainter linkchecker autolink lists checklist media mediaembed pageembed permanentpen powerpaste table advtable tinycomments tinymcespellchecker',
          toolbar: 'a11ycheck addcomment showcomments casechange checklist code export formatpainter pageembed permanentpen table',
          toolbar_mode: 'floating',
          tinycomments_mode: 'embedded',
          tinycomments_author: 'Author name',
        });
    </script>
@endpush
@endsection
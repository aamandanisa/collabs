@extends('layout.master')

@section('judul')
    Detail Film
@endsection

@section('subjudul')
    Halaman detail dari data suatu film
@endsection

@section('content')
    <div class="row">
        <div class="col-4">
            <div class="card">
                <img src="{{asset('image/'.$film->poster)}}" width="100%" height="300px" class="card-image-top "alt="...">
                <div class="card-body">
                    <h3>{{$film->judul}}</h3>
                    <h5>{{$film->tahun}}</h5>
                    <p class="card-text">{{$film->ringkasan}}</p>

                    <div>
                        <h2>Cast</h2>
                
                        <hr>
                
                         @forelse($film->peran as $item2)
                        <div class="card">
                            <div class="card-header">
                              {{$item2->cast->name}}
                            </div>
                            <div class="card-body">
                              <p class="card-text">{{$item2->nama}}</p>
                            </div>
                          </div>
                            
                        @empty
                             <h3>Belum ada cast di film ini</h3>
                        @endforelse

                     
                
                        <form action="/peran" method="POST">
                            @csrf
                                <div class="form-group">
                                    <input type="hidden" value="{{$film->id}}" name="film_id">
                                    <textarea name="nama" class="form-control" placeholder="Isi Peran di Film"></textarea>
                                    <select name="cast_id" class="form-control">
                                        <option value="">---Pilih Cast---</option>
                                        @foreach ($cast as $item2)
                                            <option value="{{$item2->id}}">{{$item2->name}}</option>
                                        @endforeach
                                    </select>

                                </div>
                               
                                @error('nama')
                                    <div class="alert alert-danger">{{ $message }}</div>
                
                                @enderror
                                <button type="submit" class="btn btn-primary">Tambah Cast</button>
                            </form>
                
                    </div>

                    {{-- <div class="form-group">
                        <label>Cast</label>
                        <select name="cast_id" class="form-control">
                            <option value="">---Pilih Cast---</option>
                            @foreach ($cast as $item2)
                                <option value="{{$item2->id}}">{{$item2->name}}</option>
                            @endforeach
                        </select>
                    </div> --}}

                      
                    <hr>
                    <a href="/film/" class="btn btn-primary">Kembali</a>
                </div>
            </div>
        </div>
    </div> 
    
    <div>
        <h2>List Kritik</h2>

        <hr>

        @forelse($film->kritik as $item)
        <div class="card">
            <div class="card-header">
              {{$item->user->name}}
            </div>
            <div class="card-body">
              <p class="card-text">{{$item->content}}</p>
            </div>
          </div>
            
        @empty
             <h3>Belum ada komentar di film ini</h3>
        @endforelse

        <form action="/kritik" method="POST">
            @csrf
                <div class="form-group">
                    <input type="hidden" value="{{$film->id}}" name="film_id">
                    <textarea name="content" class="form-control" placeholder="Isi kritik Anda"></textarea>
                </div>
                @error('content')
                    <div class="alert alert-danger">{{ $message }}</div>

                @enderror
                <button type="submit" class="btn btn-primary">Tambah Kritik</button>
            </form>

    </div>
    {{-- <h1>{{$film->judul}}</h1>
    <h3>{{$film->tahun}}</h3>
    <img src="{{$film->poster}}">
    <p>{{$film->ringkasan}}</p> --}}

@endsection
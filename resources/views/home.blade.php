@extends('layout.master')

@section('content')
<div class="card bg-dark text-white"> 
    <div class="card mb-10">   
            <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
                      <ol class="carousel-indicators">
                        <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
                        <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
                        <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>                   
                      </ol>
                    <div class="carousel-inner">
                      <div class="carousel-item active">
                        <img class="d-block w-100" img src="{{asset('template/images/home/a.jpg')}}" alt="First slide">
                      </div>
                      <div class="carousel-item">
                          <img class="d-block w-100" img src="{{asset('template/images/home/b.jpg')}}" alt="Second slide">
                        </div>
                        <div class="carousel-item">
                          <img class="d-block w-100" img src="{{asset('template/images/home/c.jpg')}}" alt="Third slide">
                        </div>                    
                      <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
                          <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                          <span class="sr-only">Previous</span>
                        </a>
                        <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
                          <span class="carousel-control-next-icon" aria-hidden="true"></span>
                          <span class="sr-only">Next</span>
                        </a>
                      </div>
                  </div>        
                </div>
            </div>
        </div>  
    
    <div class="card-img-overlay">
      <h2 class="card-title">Welcome to Focus, Inc</h2>      
        <p class="card-text">Unlimited movies. Watch anywhere. Cancel anytime.
        Ready to watch? Register now.</p>
    </div>
  </div>
  <h2 class="card-title">KIDS MOVIES</h2>
  <div class="row">        
    <div class="col-sm-2">
      <div class="card">
        <div class="card-body">         
          <img class="card-img" img src="{{asset('template/images/home/kids/a.png')}}" alt="Card image">
        </div>
      </div>
    </div>
    <div class="col-sm-2">
      <div class="card">
        <div class="card-body">
          
          <img class="card-img" img src="{{asset('template/images/home/kids/b.jpg')}}" alt="Card image">
        </div>
      </div>
    </div>
        <div class="col-sm-2">
          <div class="card">
            <div class="card-body">
     
              <img class="card-img" img src="{{asset('template/images/home/kids/c.jpg')}}" alt="Card image">
            </div>
          </div>
        </div>
        <div class="col-sm-2">
          <div class="card">
            <div class="card-body">
   
              <img class="card-img" img src="{{asset('template/images/home/kids/d.jpg')}}" alt="Card image">
            </div>
          </div>
        </div>
    <div class="col-sm-2">
        <div class="card">
          <div class="card-body">
  
            <img class="card-img" img src="{{asset('template/images/home/kids/e.webp')}}" alt="Card image">
          </div>
        </div>
      </div>
      <div class="col-sm-2">
        <div class="card">
          <div class="card-body">            
            <img class="card-img" img src="{{asset('template/images/home/kids/f.webp')}}" alt="Card image">
          </div>
        </div>
      </div>

      <h2 class="card-title">TEEN MOVIES</h2>
  <div class="row">        
    <div class="col-sm-2">
      <div class="card">
        <div class="card-body">         
          <img class="card-img" img src="{{asset('template/images/home/teen/a.jpeg')}}" alt="Card image">
        </div>
      </div>
    </div>
    <div class="col-sm-2">
      <div class="card">
        <div class="card-body">
          
          <img class="card-img" img src="{{asset('template/images/home/teen/b.jpeg')}}" alt="Card image">
        </div>
      </div>
    </div>
        <div class="col-sm-2">
          <div class="card">
            <div class="card-body">
     
              <img class="card-img" img src="{{asset('template/images/home/teen/c.jpg')}}" alt="Card image">
            </div>
          </div>
        </div>
        <div class="col-sm-2">
          <div class="card">
            <div class="card-body">
   
              <img class="card-img" img src="{{asset('template/images/home/teen/d.jpg')}}" alt="Card image">
            </div>
          </div>
        </div>
    <div class="col-sm-2">
        <div class="card">
          <div class="card-body">
  
            <img class="card-img" img src="{{asset('template/images/home/teen/e.jpg')}}" alt="Card image">
          </div>
        </div>
      </div>
      <div class="col-sm-2">
        <div class="card">
          <div class="card-body">
            
            <img class="card-img" img src="{{asset('template/images/home/teen/f.jpg')}}" alt="Card image">
          </div>
        </div>
      </div>
  
      <h2 class="card-title">ADULT MOVIES</h2>
  <div class="row">        
    <div class="col-sm-2">
      <div class="card">
        <div class="card-body">         
          <img class="card-img" img src="{{asset('template/images/home/adult/a.jpg')}}" alt="Card image">
        </div>
      </div>
    </div>
    <div class="col-sm-2">
      <div class="card">
        <div class="card-body">
          
          <img class="card-img" img src="{{asset('template/images/home/adult/b.jpg')}}" alt="Card image">
        </div>
      </div>
    </div>
        <div class="col-sm-2">
          <div class="card">
            <div class="card-body">
     
              <img class="card-img" img src="{{asset('template/images/home/adult/c.jpg')}}" alt="Card image">
            </div>
          </div>
        </div>
        <div class="col-sm-2">
          <div class="card">
            <div class="card-body">
   
              <img class="card-img" img src="{{asset('template/images/home/adult/d.jpg')}}" alt="Card image">
            </div>
          </div>
        </div>
    <div class="col-sm-2">
        <div class="card">
          <div class="card-body">
  
            <img class="card-img" img src="{{asset('template/images/home/adult/e.jpg')}}" alt="Card image">
          </div>
        </div>
      </div>
      <div class="col-sm-2">
        <div class="card">
          <div class="card-body">            
            <img class="card-img" img src="{{asset('template/images/home/adult/f.jpg')}}" alt="Card image">
          </div>
        </div>
      </div>
      
   

          @if (session('status'))
          <div class="alert alert-success" role="alert">
              {{ session('status') }}
          </div>
      @endif       
                         
        </div>
    </div>
</div>
@endsection

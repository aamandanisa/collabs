@extends('layout.master')

@section('judul')
    Edit Cast
@endsection

@section('subjudul')
    Halaman untuk mengubah data cast
@endsection

@section('content')
    <form action="/cast/{{$cast->id}}" method="POST">
    @csrf
    @method('put')
        <div class="form-group">
            <label>Nama Cast</label>
            <input value="{{$cast->name}}" type="text" class="form-control" name="name">
        </div>
        @error('name')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror
        <div class="form-group">
            <label>Umur</label>
            <input value="{{$cast->umur}}" type="number" class="form-control" name="umur">
        </div>
        @error('umur')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror
        <div class="form-group">
            <label>Bio</label>
            <textarea name="bio" class="form-control">{{$cast->bio}}</textarea>
        </div>
        @error('bio')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror
        <button type="submit" class="btn btn-primary">Edit</button>
    </form>
@endsection
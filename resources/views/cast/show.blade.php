@extends('layout.master')

@section('judul')
    Detail Cast
@endsection

@section('subjudul')
    Halaman detail dari data suatu cast
@endsection

@section('content')
    <h1>{{$cast->name}}</h1>
    <h3>{{$cast->umur}}</h3>
    <p>{{$cast->bio}}</p>

<a href="/cast" class="btn btn-secondary btn-sm">Kembali</a>
@endsection

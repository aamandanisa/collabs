@extends('layout.master')

@section('judul')
    List Cast
@endsection

@section('subjudul')
    Menampilkan seluruh cast
@endsection

@section('content')
    <a href="/cast/create" class="btn btn-primary my-3"> Tambah Cast </a>

    <table class="table">
        <thead class="thead-dark">
          <tr>
            <th scope="col">#</th>
            <th scope="col">Nama Cast</th>
            <th scope="col">Action</th>
          </tr>
        </thead>
        <tbody>
            @forelse ($cast as $key=>$item)
                <tr>
                    <td>{{$key + 1}}</td>
                    <td>{{$item->name}}</td>
                    <td>
                        <form action="/cast/{{$item->id}}" method="post">
                            @csrf
                            @method('delete')
                            <a href="/cast/{{$item->id}}" class="btn btn-info btn-sm">Detail</a>
                            <a href="/cast/{{$item->id}}/edit" class="btn btn-warning btn-sm">Edit</a>
                            <input type="submit" value="Delete" class="btn btn-danger btn-sm">
                        </form>
                    </td>
                </tr>
            @empty
                <tr>
                    <td>Data tidak ada</td>
                </tr>
            @endforelse
        </tbody>
      </table>

@endsection
@extends('layout.master')

@section('judul')
    Tambah Cast
@endsection

@section('subjudul')
    Halaman untuk menambahkan data cast
@endsection

@section('content')
    <form action="/cast" method="POST">
    @csrf
        <div class="form-group">
            <label>Nama Cast</label>
            <input type="text" class="form-control" name="name">
        </div>
        @error('name')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror
        <div class="form-group">
            <label>Umur</label>
            <input type="number" class="form-control" name="umur">
        </div>
        @error('umur')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror
        <div class="form-group">
            <label>Biodata</label>
            <textarea name="bio" class="form-control"></textarea>
        </div>
        @error('bio')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror
        <button type="submit" class="btn btn-primary">Tambah</button>
    </form>
@endsection
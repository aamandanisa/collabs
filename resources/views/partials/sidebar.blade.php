<div class="quixnav">
    <div class="quixnav-scroll">
        <ul class="metismenu" id="menu">
            <li class="nav-label first">Main Menu</li>
            <!-- <li><a href="index.html"><i class="icon icon-single-04"></i><span class="nav-text">Dashboard</span></a>
            </li> -->
            {{-- sidebar --}}
            <div class ="sidebar">

            {{-- <li><a class="nav-item" href="/home" aria-expanded="false"><i
                    class="icon icon-home"></i><span class="nav-text">Home</span></a> --}}

                <!-- Sidebar user (optional) -->
      <div class="user-panel mt-3 pb-3 mb-3 d-flex">
        <div class="image">
          <img src="{{asset('template/images/avatar/2.png')}}" class="img-circle elevation-2" alt="User Image">
        </div>
        @auth
        <div class="info">
          <a href="/login" class="d-block">{{Auth::user()->name}} ({{Auth::user()->profile->umur}})</a>
        </div> 
        @endauth
        @guest
         <div class="info">
          <a href="/login" class="d-block">Halaman Guest</a>
        </div>   
        @endguest
        
      </div>

            </div>
            

                   
            </li>

            <li class="nav-label">Table</li>
            <li><a class="has-arrow" href="javascript:void()" aria-expanded="false"><i
                        class="icon icon-app-store"></i><span class="nav-text">Table</span></a>
                <ul aria-expanded="false">
                    <li><a href="/genre">Genre</a></li>
                    <li><a href="/film">Film</a></li>
                    <li><a href="/cast">Cast</a></li>
                    <li><a href="/profile">Profile</a></li>
                    @auth
                    <li class="nav-item bg-danger"><a href="{{route('logout')}}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();" >Log Out</a></li>
                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                        @csrf
                    </form>
                    @endauth

                    @guest
                    <li class="nav-item bg-primary"><a href="/login">Login</a></li>
                    @endguest

                    

                </ul>
            </li>                 
        </ul>
    </div>
</div>
@extends('layout.master')

@section('judul')
    List Peran
@endsection

@section('subjudul')
    Menampilkan seluruh peran
@endsection

@section('content')
    <a href="/peran/create" class="btn btn-primary my-3"> Tambah Peran </a>

    <table class="table">
        <thead class="thead-dark">
          <tr>
            <th scope="col">#</th>
            <th scope="col">Nama Peran</th>
            <th scope="col">Action</th>
          </tr>
        </thead>
        <tbody>
            @forelse ($peran as $key=>$item)
                <tr>
                    <td>{{$key + 1}}</td>
                    <td>{{$item->nama}}</td>
                    <td>
                        <form action="/peran/{{$item->id}}" method="post">
                            @csrf
                            @method('delete')
                            <a href="/peran/{{$item->id}}" class="btn btn-info btn-sm">Detail</a>
                            <a href="/peran/{{$item->id}}/edit" class="btn btn-warning btn-sm">Edit</a>

                            <input type="submit" value="Delete" class="btn btn-danger btn-sm">
                        </form>
                        {{-- /peran/{pern_id}/edit', 'PeranController@edit --}}

                    </td>
                </tr>
            @empty
                <tr>
                    <td>Data tidak ada</td>
                </tr>
            @endforelse
        </tbody>
      </table>

@endsection
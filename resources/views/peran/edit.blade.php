@extends('layout.master')

@section('judul')
    Edit Peran
@endsection

@section('subjudul')
    Halaman untuk mengubah data peran
@endsection

@section('content')
    <form action="/peran/{{$peran->id}}" method="POST">
    @csrf
    @method('put')
        <div class="form-group">
            <label>Nama Peran</label>
            <input value="{{$peran->nama}}" type="text" class="form-control" name="nama">
        </div>
        @error('nama')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror
        <button type="submit" class="btn btn-primary">Edit</button>
    </form>
@endsection
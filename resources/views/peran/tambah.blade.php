@extends('layout.master')

@section('judul')
    Tambah Peran
@endsection

@section('subjudul')
    Halaman untuk menambahkan data peran
@endsection

@section('content')
    <form action="/peran" method="POST">
    @csrf
        <div class="form-group">
            <label>Nama Peran</label>
            <input type="text" class="form-control" name="nama">
        </div>
        @error('nama')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror
        <button type="submit" class="btn btn-primary">Tambah</button>
    </form>
@endsection
@extends('layout.master')

@section('judul')
	Halaman Detail Peran  {{$peran->nama}}
@endsection

@section('content')

<h3>{{$peran->nama}} <br><br></h3>
	<div class="row">
@forelse ($peran->film as $item)
        <div class="col-4">
            <div class="card">
                <img src="{{asset('image/'.$item->poster)}}" width="100%" height="300px" alt="...">
                <div class="card-body">
                    <h3>{{$item->judul}}</h3>
                    <p class="card-text">{{Str::limit($item->ringkasan,100)}}</p>
                    
                    <a href="/film/{{$item->id}}" class="btn btn-primary">Detail</a>

                </div>
            </div>
        </div>    
        @empty
        <h1>Tidak ada cast di peran ini</h1>

        @endforelse
    </div>

		
		<a href="/peran/" class="btn btn-info mt-3 ">Kembali</a>
	






@endsection

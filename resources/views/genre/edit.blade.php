@extends('layout.master')

@section('judul')
    Edit Genre
@endsection

@section('subjudul')
    Halaman untuk mengubah data genre
@endsection

@section('content')
    <form action="/genre/{{$genre->id}}" method="POST">
    @csrf
    @method('put')
        <div class="form-group">
            <label>Nama Genre</label>
            <input value="{{$genre->nama}}" type="text" class="form-control" name="nama">
        </div>
        @error('nama')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror
        <button type="submit" class="btn btn-primary">Edit</button>
    </form>
@endsection
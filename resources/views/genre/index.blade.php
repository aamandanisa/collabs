@extends('layout.master')

@section('judul')
    List Genre
@endsection

@section('subjudul')
    Menampilkan seluruh genre
@endsection

@section('content')
    <a href="/genre/create" class="btn btn-primary my-3"> Tambah Genre </a>

    <table class="table">
        <thead class="thead-dark">
          <tr>
            <th scope="col">#</th>
            <th scope="col">Nama Genre</th>
            <th scope="col">Action</th>
          </tr>
        </thead>
        <tbody>
            @forelse ($genre as $key=>$item)
                <tr>
                    <td>{{$key + 1}}</td>
                    <td>{{$item->nama}}</td>
                    <td>
                        <form action="/genre/{{$item->id}}" method="post">
                            @csrf
                            @method('delete')
                            <a href="/genre/{{$item->id}}" class="btn btn-info btn-sm">Detail</a>
                            <a href="/genre/{{$item->id}}/edit" class="btn btn-warning btn-sm">Edit</a>

                            <input type="submit" value="Delete" class="btn btn-danger btn-sm">
                        </form>
                        {{-- /genre/{genre_id}/edit', 'GenreController@edit --}}

                    </td>
                </tr>
            @empty
                <tr>
                    <td>Data tidak ada</td>
                </tr>
            @endforelse
        </tbody>
      </table>

@endsection
@extends('layout.master')

@section('judul')
    Tambah Genre
@endsection

@section('subjudul')
    Halaman untuk menambahkan data genre
@endsection

@section('content')
    <form action="/genre" method="POST">
    @csrf
        <div class="form-group">
            <label>Nama Genre</label>
            <input type="text" class="form-control" name="nama">
        </div>
        @error('nama')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror
        <button type="submit" class="btn btn-primary">Tambah</button>
    </form>
@endsection
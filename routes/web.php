<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/master', function () {
    return view('layout.master');
});

Route::get('/home', function (){
    return view('home');
});
// Auth::routes();

// Route::get('/home', 'HomeController@index')->name('home');

// CRUD Cast //
Route::group(['middleware' => ['auth']], function () {
//Create Data
//mengarah ke form tambah cast
Route::get('/cast/create', 'CastController@create');
//menyimpan data ke datablase
Route::post('/cast','CastController@store');

//Read Data 
// menampilkan semua data ke database 
Route::get('/cast','CastController@index');
//route detail cast
Route::get('/cast/{cast_id}','CastController@show');

//update data
//mengarah ke form edit cast yang membawa parameter
Route::get('/cast/{cast_id}/edit', 'CastController@edit');
//update data di table cast berdasarkan id
Route::put('/cast/{cast_id}','CastController@update');


});



//delete data
//menghapus data di cast berdasarkan id
Route::delete('/cast/{cast_id}','CastController@destroy');

// CRUD Peran //
//mengarah ke form tambah peran
Route::get('/peran/create', 'PeranController@create');
//menyimpan data ke datablase
Route::post('/peran','PeranController@store');

//Read Data 
// menampilkan semua data ke database 
Route::get('/peran','PeranController@index');
//route detail genre
Route::get('/peran/{peran_id}','PeranController@show');



//update data
//mengarah ke form edit peran yang membawa parameter
Route::get('/peran/{peran_id}/edit', 'PeranController@edit');
//update data di table cast berdasarkan id
Route::put('/genre/{peran_id}','PeranController@update');

//delete data
//menghapus data di peran berdasarkan id
Route::delete('/genre/{peran_id}','PeranController@destroy');

// CRUD Genre //
//mengarah ke form tambah genre
Route::get('/genre/create', 'GenreController@create');
//menyimpan data ke datablase
Route::post('/genre','GenreController@store');

//Read Data 
// menampilkan semua data ke database 
Route::get('/genre','GenreController@index');
//route detail genre
Route::get('/genre/{genre_id}','GenreController@show');



//update data
//mengarah ke form edit genre yang membawa parameter
Route::get('/genre/{genre_id}/edit', 'GenreController@edit');
//update data di table cast berdasarkan id
Route::put('/genre/{genre_id}','GenreController@update');

//delete data
//menghapus data di genre berdasarkan id
Route::delete('/genre/{genre_id}','GenreController@destroy');

//profile
Route::resource('profile','ProfileController')->only([
    'index', 'update'
]);

Route::post('/kritik','KritikController@store');


// CRUD Film //
//mengarah ke form tambah genre
Route::get('/film/create', 'FilmController@create');
//menyimpan data ke datablase
Route::post('/film','FilmController@store');

//Read Data 
// menampilkan semua data ke database 
Route::get('/film','FilmController@index');
//route detail genre
Route::get('/film/{film_id}','FilmController@show');

//update data
//mengarah ke form edit genre yang membawa parameter
Route::get('/film/{film_id}/edit', 'FilmController@edit');
//update data di table cast berdasarkan id
Route::put('/film/{film_id}','FilmController@update');

//delete data
//menghapus data di genre berdasarkan id
Route::delete('/film/{film_id}','FilmController@destroy');

Route::resource('film','FilmController');

Auth::routes();
Route::get('/home', 'HomeController@index')->name('home');

//route laravel excel
Route::get('genre/export/', 'GenreController@export');

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cast extends Model
{
    protected $table = 'cast';

    protected $fillable = ['name','umur','bio'];

    public function peran()
    {
        return $this->hasMany('App\Peran', 'cast_id');
    }

    
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Film;

class Peran extends Model
{
    protected $table = 'peran';

    protected $fillable = ['film_id','cast_id','nama'];

    public function cast()
    {
        return $this->belongsTo('App\Cast', 'cast_id');
    }

    public function film()
    {
        return $this->belongsTo('App\Film', 'film_id');
    }
}

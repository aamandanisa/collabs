<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Genre;
use App\Exports\GenreExport;
use Maatwebsite\Excel\Facades\Excel;
use RealRashid\SweetAlert\Facades\Alert;
class GenreController extends Controller
{
    //genre laravel exxcel
    public function export() 
    {
        return Excel::download(new GenreExport, 'genre.xlsx');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $genre = Genre::all();
        return view('genre.index', compact('genre'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('genre.tambah');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request-> validate([
            'nama' => 'required',
        ],
        [
            'nama.required' =>'inputan nama  harus di isi',
        ]
        );
    
        $genre = new Genre;
 
        $genre->nama = $request->nama;

        $genre->save();
        Alert::success('Berhasil', 'Berhasil menambahkan genre baru');
        return redirect('/genre');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $genre = Genre::find($id);
        return view('genre.show', compact('genre'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $genre = Genre::find($id);
        return view('genre.edit', compact('genre'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request-> validate([
            'nama' => 'required',

        ],
        [
            'nama.required' =>'inputan nama harus di isi',
        ]
        );
        $genre = Genre::find($id);

        $genre->nama = $request->nama;

        $genre->save();
        Alert::success('Berhasil', 'Berhasil mengupdate genre');
        return redirect('/genre');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $genre = Genre::find($id);

        $genre->delete();
        Alert::success('Berhasil', 'Berhasil menghapus genre');
        return redirect('/genre');
    }
}

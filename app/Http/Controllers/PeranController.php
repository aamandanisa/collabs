<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Peran;
use App\Cast;

class PeranController extends Controller
{

    public function index()
    {
        $peran = Peran::all();
        return view('peran.index', compact('peran'));
    }

    public function create()
    {
        return view('peran.tambah');
    }
   
    public function store(Request $request)
    {
       $request-> validate([
        'nama' => 'required',
        'cast_id' => 'required',
        
    ]);

    $peran = new Peran;

    $peran->nama = $request->nama;
    $peran->film_id = $request->film_id;
    $peran->cast_id = $request->cast_id;

    $peran->save();

    return redirect('/film/'.$request->film_id); 
    }
    

    public function show($id)
    {
        $peran = Peran::find($id);
        return view('peran.show', compact('peran'));
    }

}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use RealRashid\SweetAlert\Facades\Alert;
use App\Film;
use App\Genre;
use App\Peran;
use App\Cast;
use File;


class FilmController extends Controller
{

    public function __construct()
    {
 
        $this->middleware('auth')->except('index','show');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {$film = Film::all();
        return view('film.index', compact('film'));
        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $genre = Genre::all();
        $cast = Cast::all();
        return view('film.tambah', compact('genre','cast'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request-> validate([
            'judul' => 'required',
            'ringkasan' => 'required',
            'tahun' => 'required',
            'poster' => 'required|image|mimes:jpeg,png,jpg|max:2048',
            'genre_id' => 'required',
        ],
        [
            'judul.required' =>'judul harus di isi',
            'ringkasan.required' =>'ringkasan harus di isi',
            'tahun.required' =>'tahun harus di isi',
            'poster.required' =>'poster harus di isi',
            'genre_id.required' =>'genre harus di pilih',
        ]
        );
        $imageName = time().'.'.$request->poster->extension();

        $request->poster->move(public_path('image'), $imageName);

        $film = new Film;
 
        $film->judul = $request->judul;
        $film->ringkasan = $request->ringkasan;
        $film->tahun = $request->tahun;
        $film->poster = $imageName;
        $film->genre_id = $request->genre_id;
 
        $film->save();
        Alert::success('Berhasil', 'Berhasil menambahkan data film baru');
        return redirect('/film');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $film = Film::find($id);
        $cast= Cast::all();
        return view('film.show', compact('film','cast'));

        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $film = Film::find($id);
        $genre = Genre::all();
        $cast = Cast::all();

        return view('film.edit', compact('film','genre','cast'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request-> validate([
            'judul' => 'required',
            'ringkasan' => 'required',
            'tahun' => 'required',
            'poster' => 'required',
            'genre_id' => 'required',
        ],
        [
            'judul.required' =>'inputan judul harus di isi',
            'ringkasan.required' =>'inputan ringkasan harus di isi',
            'tahun.required' =>'inputan tahun harus di isi',
            'poster.required' =>'inputan poster harus di isi',
            'genre_id.required' =>'genre harus di pilih',
        ]
        );
        $film = Film::find($id);

        if ($request->has('poster')){
            $path = "image/";
            File::delete($path . $film->poster);
            $imageName = time().'.'.$request->poster->extension();
            $request->poster->move(public_path('image'), $imageName);
            $film->poster = $imageName;
        }
 
        $film->judul = $request->judul;
        $film->ringkasan = $request->ringkasan;
        $film->tahun = $request->tahun;
        $film->genre_id = $request->genre_id;
        $film->save();
        Alert::success('Berhasil', 'Berhasil merubah data film');
        return redirect('/film');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $film = Film::findorfail($id);
        $film->delete();

        $path = "image/";
        File::delete($path . $film->poster);
        Alert::success('Berhasil', 'Berhasil menghapus data film');
        return redirect('/film');
    }
}

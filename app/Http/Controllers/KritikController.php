<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Kritik;

class KritikController extends Controller
{
    public function store(Request $request)
    {
       $request-> validate([
        'content' => 'required',
    ]);

    $kritik = new Kritik;

    $kritik->content = $request->content;
    $kritik->film_id = $request->film_id;
    $kritik->user_id = Auth::id();

    $kritik->save();

    return redirect('/film/'.$request->film_id); 
    }
    
}

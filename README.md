## **Final Project**

## Kelompok 2

## Anggota Kelompok

-   Amira Amandanisa 
-   Anne Mumtaza Putri	
-   Roni Sudarwan

## Tema Project

Aplikasi Review Film

## ERD

<p align="center"><a href="#"><img src="public/template/images/erd.jpg" width="100%"></a></p>

## Deskripsi

-   Template : Menggunakan template dari Focus-Bootstrap 4 HTML5 Admin Dashboard
-   Membuat crud cast dan genre (menambahkan artis dan genre film)
-   Demo : Demo yang ditunjukkan pada video berupa proses authentification (registrasi/login), crud (manipulasi data film),  menambahkan peran dan menampilkan fitur eloquent dengan menambahkan komentar menggunakan akun teregistrasi pada postingan beberapa film.


## Link Video

-   Link Demo Aplikasi : https://www.youtube.com/watch?v=MPnTDzjw6uo
-   Link Deploy(optional) : https://finpro-laravel-cda.sanbercodeapp.com/
